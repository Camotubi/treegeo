@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Administración de usuarios</div>

                <div class="card-body">
                    <table class="table">
                        <tr>
                            <th>Nombre</th>
                            <th>E-mail</th>
                            <th>Tipo</th>
                        </tr>
                        @foreach($users as $user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                <form action="/user/{{$user->id}}" method="POST">
                                {{ csrf_field() }}
                                @method('PATCH')
                                    <select name="type" id="type" required>
                                    @foreach($userTypes as $userType)
                                        @if($user->type()->first()->id == $userType->id)
                                        <option value="{{$userType->id}}" selected>{{$userType->name}}</option>
                                        @else
                                        <option value="{{$userType->id}}">{{$userType->name}}</option>
                                        @endif
                                    @endforeach
                                    </select> 
                                    <input type="submit" class="btn btn-primary btn-sm ml-2" name="update" value="Modificar">
                                    <a class="btn btn-danger btn-sm ml-2" href="#user_delete" data-toggle="modal" data-target="#user_delete">Eliminar</a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="user_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Eliminación de usuario</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                </div>
                                                <form class="" action="/user/{{$user->id}}" method="post">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="delete">
                                                    <div class="modal-body">
                                                         <p>Desea eliminar al usuario {{$user->name}}?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="submit" class="btn btn-danger" name="destroy" value="Eliminar" data-dismiss="modal">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                            </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection