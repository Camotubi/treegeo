<nav class="col col-2 col-md-2 d-none d-md-block bg-light sidebar">
  <div class="sidebar-sticky">
    <div class="text-center">
    </div>
    <ul class="nav flex-column">
      <li class="nav-item">
        <a href="{{ url('/') }}" class="nav-link active">Mapa</a>
      </li>
      <li class="nav-item">
        <a href="{{ url('/admin/tree') }}" class="nav-link active">Árboles</a>
      </li>
      <li>
        <a href="{{ url('/admin/trail') }}" class="nav-link active">Senderos</a>
      </li>
    </ul>
  </div>
</nav>
