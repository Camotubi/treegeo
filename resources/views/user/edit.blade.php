@extends('layouts.app')

@section('content')
	<div>
		<section>
			<article>
				<h3>Actualización de Tipo de Usuario</h3>
				<form class=""  action= "/user/{{$user->id}}" method="post">
					{{ csrf_field() }}
					{{ method_field('PATCH') }}
					<div class="form-row">
						<div class="form-group col-md-4">
                            <label for="name">Nombre:</label>
                            <p>{{$user->name}}</p>
							<!--<input class="form-control" type="text" name="name" value="{{$user->name}}">-->
						</div>
						<div class="form-group col-md-4">
                            <label for="email">E-mail:</label>
                            <p>{{$user->email}}</p>
							<!--<input class="form-control" type="email" name="email" value="{{$user->email}}">-->
						</div>
						<div class="form-group col-md-4">
                            <label for="type">Tipo de Usuario:</label>
							<select name="type" id="type" required>
								@foreach($userTypes as $userType)
									@if($user->type()->first()->id == $userType->id)
										<option value="{{$userType->id}}" selected>{{$userType->name}}</option>
									@else
										<option value="{{$userType->id}}">{{$userType->name}}</option>
									@endif
								@endforeach
							</select> 
							<!--<input class="form-control" type="text" name="type" value="{{$user->type}}">-->
						</div>
					</div>
					<input type="submit" class="btn btn-primary" name="update" value="Actualizar">
				</form>
			</article>
		</section>
	</div>

	</div>
@endsection