@extends('layouts.app') 
@section('content')
<div>
    <section>
        <article>
            <h3>Actualización de posición</h3>
            <form class="" action="{{$action}}" method="post">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-group col-md-4">
                    <label for="lat">Latitud:</label>
                    <input class="form-control" type="text" name="lat" value="{{$position->lat}}">
                </div>
                <div class="form-group col-md-4">
                    <label for="lng">Longitud:</label>
                    <input class="form-control" type="text" name="lng" value="{{$position->lng}}">
                </div>
                <input type="hidden" name="parent_type" value="{{$parent_type}}" />
                <input type="hidden" name="parent_id" value="{{$parent_id}}" />
                <input type="submit" class="btn btn-primary" name="update" value="Actualizar">
            </form>
        </article>
    </section>
</div>
@endsection