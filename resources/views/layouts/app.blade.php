<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>


  <!-- Fonts -->
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/custom.css')}}"> @yield('style')
  <script src="{{ asset('js/app.js') }}" defer></script>

</head>

<body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDengzWSmEKFCk7wyV4r7CflDb8_LStKII&amp;sensor=false"></script>
  @yield('script')
  <div id="app">
    <nav class="navbar navbar-expand-lg navbar-light thickmenu" style="background-color: #e3f2fd;">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">
            GEO TREE
          </a>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto mr-0 mt-2 mt-lg-0">
          @guest
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/login') }}">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/register') }}">Register</a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                   Logout
                  </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
          </li>
          <li class="nav-item">
            <span class="nav-link">{{ Auth::user()->name }}</span>
          </li>
          @endguest
        </ul>
      </div>
    </nav>

    <div class="container-fluid h-100">
      <div class="row justify-content-center full-screen">
        @guest
        @include('pages.guest_menu') 
        @elseif(Auth::User()->isAdmin())
        @include('pages.admin_menu')
        @else
        @include('pages.user_menu')
        @endguest

        <main class="col col-9 col-md-9 ml-sm-auto col-lg-10 px-4 bg-white" style="padding:0 0 !important;">
          <div class="container pt-2">
  @include('components.message')
          </div>
          @yield('content')
        </main>
      </div>
    </div>
  </div>


</body>

</html>