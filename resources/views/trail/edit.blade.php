@extends('layouts.app')

@section('content')
	<div>
		<section>
			<article>
				<h3>Actualización de datos de sendero</h3>
				<form class=""  action= "/admin/trail/{{$trail->id}}" method="post">
					{{ csrf_field() }}
					{{ method_field('PATCH') }}
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="name">Nombre:</label>
						<input class="form-control" type="text" name="name" value="{{$trail->name}}">
						</div>
						<div class="form-group col-md-4">
							<label for="description">Descripción:</label>
							<textarea class="form-control" name="description" rows="8" cols="80">{{$trail->description}}</textarea>
						</div>
						<div class="form-group col-md-4">
							<label for="group_id">Grupo de coordenadas:</label>
							<select name="group_id" id="group_id">
								@foreach($groupIds as $groupId)
									<option value="{{$groupId}}">{{$groupId}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<input type="submit" class="btn btn-primary" name="update" value="Actualizar">
				</form>
			</article>
		</section>
	</div>
@endsection