@extends('layouts.app') 
@section('content')
<div>
    <section>
        <article>
            <h3>Creación de datos de sendero</h3>
            <form class="" action="/admin/trail" method="post">
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="name">Nombre:</label>
                        <input class="form-control" type="text" name="name" value="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="description">Descripción:</label>
                        <textarea class="form-control" name="description" rows="8" cols="80"></textarea>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary" name="create" value="Crear">
            </form>
        </article>
    </section>
</div>
@endsection