@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Lista de senderos</div>
            </div>
            @guest 
            @elseif(Auth::User()->isAdmin())
            <div>
                <a href="/admin/trail/create" class="btn btn-primary w-100">Crear</a>
            </div>
            @else
            @endguest
            <div>
                @guest
                @include('components.trail.table',['showModifyCol' => false])
                @elseif(Auth::User()->isAdmin())
                @include('components.trail.table',['showModifyCol' => true])
                @else
                @include('components.trail.table',['showModifyCol' => false])
                @endguest
            </div>
        </div>
    </div>
</div>
@endsection
