@extends('layouts.app') 
@section('content')
<div>
    <section>
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <h3>Sendero - {{$trail->name}} </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        @if($trail->images()->where('type','primary')->count())
                        <img class="w-100" src="{{$trail->images()->where('type','primary')->first()->public_file_location}}">                        @else
                        <img class="w-100" src="{{asset('/img/simple_trail.svg')}}"> @endif
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <p class="col">
                                <strong>Nombre:</strong>
                                <span>{{$trail->name}}</span>
                            </p>
                        </div>
                        <div class="row">
                            <div class="col">
                                <strong>Descripción</strong>
                                <p>{{$trail->description}}</p>
                            </div>
                        </div>
                        @guest @else
                        <div class="row control-buttons">
                            <!-- Button trigger modal -->
                            <div class="d-flex justify-content-end w-100 pb-3 pr-3 align-bottom">
                                <a href="#positions_create" class="btn btn-primary mr-2" data-toggle="modal" data-target="#positions_create">
                                    Agregar Posiciones
                                </a>
                                <a href="#add_image" class="btn btn-primary mr-2" data-toggle="modal" data-target="#add_image">
                                    Agregar Imagen
                                </a>
                                @if($trail->positions()->where('active', true)->count())
                                <a href="#position_map" class="btn btn-primary mr-2" data-toggle="modal" data-target="#position_map">
                                    Ver Ubicación
                                </a>
                                @endif
                                @if(Auth::User()->isAdmin())
                                <a href="#trail_delete" class="btn btn-danger mr-2" data-toggle="modal" data-target="#trail_delete">
                                   Eliminar Sendero
                                </a> @endif
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="map-modal">
                            <div class="modal fade" id="position_map" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Ubicación</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
                                        </div>
                                        <div class="modal-body">
                                            <div id="mapcanvas"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="add_image" tabindex="-1" role="dialog" aria-labelledby="addImageModal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="addImageModal">Agregar Imagen</h5>
                                    </div>
                                    <form action="/image" method="post" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <label for="image_files">Imagenes:</label>
                                                <input type="file" name="images[]" id="image_files" class="form-control-file form-control-sm" multiple/>
                                            </div>
                                            <input type="hidden" name="parent_type" value="trail" />
                                            <input type="hidden" name="parent_id" value="{{$trail->id}}" />
                                        </div>
                                        <div class="modal-footer">
                                            <input type="submit" class="btn btn-primary" value="Enviar" />
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="positions_create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Creación de sendero</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <form class="" action="/trail/{{$trail->id}}/add_positions" method="post">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                    <div id="positions_form">
                                        <div class="form-group col-md-4">
                                            <label for="lat">Latitud:</label>
                                            <input class="form-control" type="text" name="lat[]" required value="">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="lng">Longitud:</label>
                                            <input class="form-control" type="text" name="lng[]" required value="">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="device">Dispositivo:</label>
                                            <input class="form-control" type="text" name="device[]" required value="">
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-primary" formnovalidate id="add">+</button>
                                    <br>
                                    <div class="form-group col-md-4">
                                    </div>
                                    <input type="hidden" name="parent_type" value="trail" />
                                    <input type="hidden" name="parent_id" value="{{$parent_id}}" />
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-primary" name="create" value="Crear">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endguest
                <!-- Modal -->
                <div class="modal fade" id="trail_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Eliminación de sendero</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                            </div>
                            <form class="" action="/trail/{{$trail->id}}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                                <div class="modal-body">
                                    <p>Desea eliminar el sendero {{$trail->name}}?</p>
                                </div>
                                <input type="hidden" name="parent_type" value="trail" />
                                <input type="hidden" name="parent_id" value="{{$parent_id}}" />
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-danger" name="destroy" value="Eliminar">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <form class="" action="/trail/{{$trail->id}}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <div class="modal-body">
                            <p>Desea eliminar el sendero {{$trail->name}}?</p>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-danger" name="destroy" value="Eliminar" data-dismiss="modal">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </form>
                </div>
            </div>
</div>
@guest
<br>
<h3>Imagenes</h3>
<div class="d-flex flex-wrap">
    @php($cont = 1) @foreach($trail->images()->get() as $image)
    <div class="order-{{$cont}} p-2">
        <img src="{{$image->public_file_location}}" style="max-height:150px; max-width:500px" alt="image">
    </div>
    @php($cont++) @endforeach
</div>
@elseif(Auth::User()->isAdmin())
    @include('components.image.table',['showModifyCol'=> true, 'images' => $trail->images()->get()])
@else
<br>
<h3>Imagenes</h3>
<div class="d-flex flex-wrap">
    @php($cont = 1) @foreach($trail->images()->get() as $image)
    <div class="order-{{$cont}} p-2">
        <img src="{{$image->public_file_location}}" style="max-height:150px; max-width:500px" alt="image">
    </div>
    @php($cont++) @endforeach
</div>
@endguest
</article>
</section>
</div>
@endsection
 
@section('script')
<script>
    @if($trail->positions()->where('active',true)->count()) 
    var map;

google.maps.event.addDomListener(window, 'load', initMap);
function initMap() {
    var tmpPositions =  {!!  $trail->positions()->where('active',true)->whereHas('positionOrders')->with('positionOrders')->get()->sortBy(function($pos,$key) { return $pos[$key]['positionOrders']['order'];})->toJson() !!};
    var positions = [];
    console.log(tmpPositions);
    for(var i= 0; i < tmpPositions.length; i++) {
        positions.push({lat:Number(tmpPositions[i].lat),lng:Number(tmpPositions[i].lng)});
    }


   var mapCanvas = document.getElementById('mapcanvas');
   var mapOptions = {
      center: new google.maps.LatLng(positions[0]),
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
   }

   map = new google.maps.Map(mapCanvas, mapOptions);
         var flightPath = new google.maps.Polyline({
          path: positions,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });
        flightPath.setMap(map);
        positions.forEach(function(el) {
            new google.maps.Marker({
    position: el,
    map: map,
    title: 'Hello World!'
  });
        } )
}
 @endif 
    $(document).ready(function(){
 @if($trail->positions()->where('active',true)->count()) 
        $("#position_map").on('shown.bs.modal', function () {
        google.maps.event.trigger(map, "resize");
        console.log()

        });
 @endif 
        $("#add").click(function() {
            console.log('mellamaron');
            $('#positions_form').append('<div class="form-group col-md-4"><label for="lat">Latitud:</label><input class="form-control" type="text" name="lat[]" required value=""></div><div class="form-group col-md-4"><label for="lng">Longitud:</label><input class="form-control" type="text" name="lng[]" required value=""></div><div class="form-group col-md-4">'+
                                            '<label for="device">Dispositivo:</label>'+
                                            '<input class="form-control" type="text" name="device[]" required value="">'+
                                        '</div>');
    });
});

</script>
@endsection
 
@section('style')
<style>
    @media (min-width: 576px) {
        div.map-modal .modal-dialog {
            max-width: none;
        }
    }

    div.map-modal .modal-dialog {
        width: 98%;
        height: 92%;
        padding: 0;
    }

    div.map-modal .modal-content {
        height: 99%;
    }

    div.map-modal .modal-body {
        display: -ms-flex;
        display: -webkit-flex;
        display: flex;
    }

    div.map-modal #mapcanvas {
        flex: 1;
    }
</style>
@endsection