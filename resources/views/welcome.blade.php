@extends('layouts.app')

@section('script')
<script src="{{ asset('js/map.js') }}" charset="utf-8"></script>
@endsection
@section('content')
  <div class="full-screen" id="">
    <div id="map"></div>
  </div>
  @foreach($treePosition as $tree)
    <span treeId="{{$tree->id}}" lat="{{$tree->positions()->first()->lat}}" long="{{$tree->positions()->first()->lng}}" title="{{$tree->common_name}}"></span>
  @endforeach
  <script type="text/javascript">

  </script>
@endsection
