<div class="table-responsive">
  <table class="table table-striped">
      <thead>
          <tr>
              <th scope="col">
                  ID
              </th>
              <th scope="col">
                  Nombre Común
              </th>
              <th scope="col">
                  Nombre Científico
              </th>
              <th scope="col">
                  Autor
              </th>
              <th scope="col">
                  Latitud
              </th>
              <th scope="col">
                  Longitud
              </th>

              @guest
                @else
                <th scope="col">
                    Activo
                </th>
                  @isset($showModifyCol) @if($showModifyCol)
                  <th scope="col">
                      Modificar
                  </th>
                  @endif @endisset
              @endguest

          </tr>
      </thead>
      @foreach($trees as $tree)
      <tr>
          <td>
              {{$tree->id}}
          </td>
          <td>
              <a href="/tree/{{$tree->id}}">{{$tree->common_name}}</a>
          </td>
          <td>
              {{$tree->scientific_name}}
          </td>
          <td>
              {{$tree->user->name}}
          </td>
          @if($tree->positions()->where('active',true)->count())
          <td>
              {{$tree->positions()->where('active',true)->first()->lat}}
          </td>
          <td>
              {{$tree->positions()->where('active',true)->first()->lng}}
          </td>
          @else
          <td>
              Sin Especificar
          </td>
          <td>
              Sin Especificar
          </td>
          @endif
          @guest
            @else
            <td>
                @if($tree->active) Sí @else No @endif
            </td>
            @isset($showModifyCol) @if($showModifyCol)
            <td>
                <a class="btn btn-primary btn-sm" href="/admin/tree/{{$tree->id}}/edit">Modificar</a>
            </td>
            @endif @endisset
          @endguest
      </tr>
      @endforeach
  </table>
</div>
