<div class="table-responsive">
  <table class="table table-striped">
      <thead>
          <tr>
              <th scope="col">
                  ID
              </th>
              <th scope="col">
                  Nombre
              </th>
              <th scope="col">
                  Autor
              </th>
              @guest
                @else
                  @isset($showModifyCol) @if($showModifyCol)
                  <th scope="col">
                      Modificar
                  </th>
                  @endif @endisset
              @endguest
          </tr>
      </thead>
      @foreach($trails as $trail)
      <tr>
          <td>
              {{$trail->id}}
          </td>
          <td>
              <a href="/trail/{{$trail->id}}">{{$trail->name}}</a>
          </td>
          <td>
              {{$trail->user->name}}
          </td>
          @guest
            @else
            @isset($showModifyCol) @if($showModifyCol)
            <td>
                <a class="btn btn-primary btn-sm" href="/admin/trail/{{$trail->id}}/edit">Modificar</a>
            </td>
            @endif @endisset
          @endguest
      </tr>
      @endforeach
  </table>
</div>
