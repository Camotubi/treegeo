<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">
                ID
            </th>
            <th scope="col">
                Imagen
            </th>
            @isset($showParent) @if($showParent)
            <th scope="col">
                Padre
            </th>
            @endif @endisset
            <th scope="col">
                Autor
            </th>
            <th scope="col">
                Activo
            </th>
            @isset($showModifyCol) @if($showModifyCol)
            <th scope="col">
                Modificar
            </th>
            @endif @endisset
        </tr>
    </thead>
    @php($cont = 0)
    @foreach($images as $image)
    <tr>
        <td>
            {{$image->id}}
        </td>
        <td >
            <img src="{{$image->public_file_location}}" style="max-height:150px; max-width:800px" alt="image">
        </td>
        <td>
            {{$image->user->name}}
        </td>
        @isset($showParent) @if($showParent)
        <td>
            PlaceHolder
        </td>
        @endif @endisset
        <td>
            @if($image->active) Sí @else No @endif
        </td>
        @isset($showModifyCol) @if($showModifyCol)
        <td>
        <form class="mb-0" action="/image/{{$image->id}}/updateActive" method="post">
                {{ csrf_field() }} {{ method_field('PATCH') }} @if($image->active)
                <input class="w-100 btn btn-secondary" type="submit" value="Desactivar">
                <input type="hidden" name="active" value="0"> @else
                <input class="w-100 btn btn-primary" type="submit" value="Activar">
                <input type="hidden" name="active" value="1"> @endif
                <input type="hidden" name="parent_type" value="{{$parent_type}}" />
                <input type="hidden" name="parent_id" value="{{$parent_id}}" />
            </form>
            <form class ="mt-2 mb-0 " action="/image/{{$image->id}}/updatePrimary" method="post">
                {{ csrf_field() }} {{ method_field('PATCH') }} @if($image->isPrimary())
                <input class="w-100 btn btn-secondary" type="submit" value="Cambiar a Secundaria">
                <input type="hidden" name="type" value="secondary"> @else
                <input class="w-100 btn btn-primary" type="submit" value="Cambiar a Primaria">
                <input type="hidden" name="type" value="primary"> @endif
                <input type="hidden" name="parent_type" value="{{$parent_type}}" />
                <input type="hidden" name="parent_id" value="{{$parent_id}}" />
            </form>
        <a class="w-100 btn btn-warning mt-2" href="#image_delete{{$cont}}" data-toggle="modal" data-target="#image_delete{{$cont}}">Eliminar</a>
            <!-- Modal -->
            <div class="modal fade" id="image_delete{{$cont}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Eliminación de imagen</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <form action="/image/{{$image->id}}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="delete">
                            <div class="modal-body">
                                <p>Desea eliminar esta imagen?</p>
                                <img src="{{$image->public_file_location}}" style="max-height:150px; max-width:400px" alt="image">
                            </div>
                            <input type="hidden" name="parent_type" value="{{$parent_type}}" />
                            <input type="hidden" name="parent_id" value="{{$parent_id}}" />
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-danger" name="destroy" value="Eliminar" >
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </td>
        @endif @endisset
    </tr>
    @php($cont++)
    @endforeach
</table>