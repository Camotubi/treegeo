<form action="{{ $action }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="image_files">Imagenes:</label>
        <input type="file" name="images[]" id="image_files" class="form-control-file form-control-sm" multiple/>
    </div>
    <input type="hidden" name="parent_type" value="{{$parent_type}}" />
    <input type="hidden" name="parent_id" value="{{$parent_id}}" />
    <input type="submit" class="btn btn-primary" value="Enviar" />
</form>