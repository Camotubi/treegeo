@extends('layouts.app')
@section('content')
<div>
	<section>
		<article>
			<h3>Actualización de datos de árbol</h3>
			<form class="" action="/admin/tree/{{$tree->id}}" method="post">
				{{ csrf_field() }} {{ method_field('PATCH') }}
				<div class="form-row">
					<div class="form-group col-md-4">
						<label for="common_name">Nombre común:</label>
						<input class="form-control" type="text" name="common_name" value="{{$tree->common_name}}">
					</div>
					<div class="form-group col-md-4">
						<label for="scientific_name">Nombre científico:</label>
						<input class="form-control" type="text" name="scientific_name" value="{{$tree->scientific_name}}">
					</div>
					<div class="form-group col-md-4">
						<label for="position">Posición:</label>
						<select name="position" id="position">
								@foreach($positions as $position)
									@if($position->active)
										<option value="{{$position->id}}" selected>{{$position->lat}} ,{{$position->lng}}</option>
									@else
										<option value="{{$position->id}}">{{$position->lat}} ,{{$position->lng}}</option>
									@endif
								@endforeach
							</select>
					</div>
					<div class="form-group col-md-4">
						<label for="description">Descripción:</label>
						<textarea class="form-control" name="description" rows="8" cols="80">{{$tree->description}}</textarea>
					</div>
					<div class="form-group col-md-4">
						<label for="active">Activo:</label> @if($tree->active)
						<input type="checkbox" name="active" value="true" checked> @else
						<input type="checkbox" name="active" value="true"> @endif
					</div>
				</div>
				<input type="submit" class="btn btn-primary" name="update" value="Actualizar">
			</form>
		</article>
	</section>
</div>
@endsection
