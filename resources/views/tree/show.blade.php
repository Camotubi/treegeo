@extends('layouts.app') 
@section('content')
<div>
    <section>
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <h3>Árbol - {{$tree->common_name}} </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        @if($tree->images()->where('type','primary')->count())
                        <img class="w-100" src="{{$tree->images()->where('type','primary')->first()->public_file_location}}">                        @else
                        <img class="w-100" src="{{asset('/img/simple_tree.svg')}}"> @endif
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <p class="col">
                                <strong>Nombre común:</strong>
                                <span>{{$tree->common_name}}</span>
                            </p>
                            <p class="col">
                                <strong>Nombre Cientifico:</strong>
                                <span>{{$tree->scientific_name}}</span>
                            </p>
                        </div>
                        <div class="row">
                            <div class="col">
                                <strong>Descripción</strong>
                                <p>{{$tree->description}}</p>
                            </div>
                        </div>
                        @guest
                        @else
                        <div class="row control-buttons">
                            <!-- Button trigger modal -->
                            <div class="d-flex justify-content-end w-100 pb-3 pr-3 align-bottom ">
                            <a href="#position_create" class="btn btn-primary mr-2" data-toggle="modal" data-target="#position_create">
                                Agregar Posición
                            </a>
                            <a href="#add_image" class="btn btn-primary mr-2" data-toggle="modal" data-target="#add_image">
                                Agregar Imagen
                            </a>
                            @if($tree->positions()->where('active', true)->count())
                            <a href="#position_map" class="btn btn-primary mr-2" data-toggle="modal" data-target="#position_map">
                                Ver ubicación
                            </a>
                            @endif
                            @if(Auth::User()->isAdmin())
                            <a href="#tree_delete" class="btn btn-danger mr-2" data-toggle="modal" data-target="#tree_delete">
                                Eliminar Árbol
                            </a>
                            @endif
                            </div>

                            <!-- Modal -->
                            <div class="map-modal">
                                <div class="modal fade" id="position_map" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Ubicación</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
                                            </div>
                                            <div class="modal-body">
                                                <div id="mapcanvas"></div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="add_image" tabindex="-1" role="dialog" aria-labelledby="addImageModal">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="addImageModal">Agregar Imagen</h5>
                                        </div>
                                        <form action="/image" method="post" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="image_files">Imagenes:</label>
                                                    <input type="file" name="images[]" id="image_files" class="form-control-file form-control-sm" multiple/>
                                                </div>
                                                <input type="hidden" name="parent_type" value="tree" />
                                                <input type="hidden" name="parent_id" value="{{$tree->id}}" />
                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" class="btn btn-primary" value="Enviar" />
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="position_create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Agregar de Posición</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                                </div>
                                <form class="" action="{{$action}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="modal-body">
                                        <div class="form-group col-md-4">
                                            <label for="lat">Latitud:</label>
                                            <input class="form-control" type="text" name="lat" value="">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="lng">Longitud:</label>
                                            <input class="form-control" type="text" name="lng" value="">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="device">Dispositivo:</label>
                                            <input class="form-control" type="text" name="device" value="">
                                        </div>
                                        <input type="hidden" name="parent_type" value="{{$parent_type}}" />
                                        <input type="hidden" name="parent_id" value="{{$parent_id}}" />
                                    </div>
                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-primary" name="create" value="Crear">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    @endguest
                    
                    <!-- Modal -->
                    <div class="modal fade" id="tree_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Eliminación de árbol</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                </div>
                                <form class="" action="/tree/{{$tree->id}}" method="post">
                                    {{ csrf_field() }} {{ method_field('DELETE') }}
                                    <div class="modal-body">
                                        <p>Desea eliminar el árbol {{$tree->common_name}}?</p>
                                    </div>
                                    <input type="hidden" name="parent_type" value="tree" />
                                    <input type="hidden" name="parent_id" value="{{$parent_id}}" />
                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-danger" name="destroy" value="Eliminar">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    @guest
    <br>
    <h3>Imagenes</h3>
    <div class="d-flex flex-wrap">
        @php($cont = 1)
        @foreach($tree->images()->get() as $image)
        <div class="order-{{$cont}} p-2">
            <img src="{{$image->public_file_location}}" style="max-height:150px; max-width:500px" alt="image">
        </div>
        @php($cont++)
        @endforeach
    </div>
    @elseif(Auth::User()->isAdmin())
    @include('components.image.table',['showModifyCol'=> true, 'images' => $tree->images()->get()])
    @else
    <br>
    <h3>Imagenes</h3>
    <div class="d-flex flex-wrap">
        @php($cont = 1) @foreach($tree->images()->get() as $image)
        <div class="order-{{$cont}} p-2">
            <img src="{{$image->public_file_location}}" style="max-height:150px; max-width:500px" alt="image">
        </div>
        @php($cont++) @endforeach
    </div>
    @endguest
        </article>
    </section>
</div>
@endsection
 @if($tree->positions()->where('active',true)->count()) 
@section('script')
<script>
    var map;

google.maps.event.addDomListener(window, 'load', initMap);
function initMap() {
    var position = {!! $tree->positions()->where('active',true)->get(['lat','lng'])->first()->toJson() !!};
    position = {lat:Number(position.lat), lng: Number(position.lng)};
   var mapCanvas = document.getElementById('mapcanvas');
   var mapOptions = {
      center: new google.maps.LatLng(position),
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
   }

   map = new google.maps.Map(mapCanvas, mapOptions);
   var marker = new google.maps.Marker({
          position: position,
          map: map,
          title: 'Hello World!'
        });
}
    $(document).ready(function(){
        $("#position_map").on('shown.bs.modal', function () {
        google.maps.event.trigger(map, "resize");
    });
});

</script>
@endsection
 @endif 
@section('style')
<style>
    @media (min-width: 576px) {
        div.map-modal .modal-dialog {
            max-width: none;
        }
    }

    div.map-modal .modal-dialog {
        width: 98%;
        height: 92%;
        padding: 0;
    }

    div.map-modal .modal-content {
        height: 99%;
    }

    div.map-modal .modal-body {
        display: -ms-flex;
        display: -webkit-flex;
        display: flex;
    }

    div.map-modal #mapcanvas {
        flex: 1;
    }

    .control-buttons {
        position: absolute;
        bottom: 20px;
        width: 100%;
    }
</style>
@endsection