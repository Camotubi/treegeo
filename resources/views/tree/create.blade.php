@extends('layouts.app') 
@section('content')
<div class="container">
    <h3>Creación de árbol</h3>
    <form action='/tree' method="POST">
        {{ csrf_field() }}
        <div class="form-row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="common_name">Nombre común:</label>
                    <input class="form-control" type="text" name="common_name" required>
                </div>
                <div class="form-group">
                    <label for="scientific_name">Nombre científico:</label>
                    <input class="form-control" type="text" name="scientific_name">
                </div>
                <div class="form-group">
                    <label for="active">Activo:</label>
                    <input type="checkbox" name="active" value="true">
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="description">Descripcción</label>
                <textarea class="form-control" name="description" rows="8" cols="80"></textarea>
            </div>
        </div>
        <input type="submit" class="btn btn-primary" name="crear" value="Crear">
    </form>
</div>
@endsection