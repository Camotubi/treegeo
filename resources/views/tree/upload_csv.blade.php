@extends('layouts.app') 
@section('content')
<div class="row justify-content-center m-4">
    <div class="col-md-8">
        <h3>Subida de Posiciones de arboles</h3>
        <div class="alert alert-info">
    <p>Para subir geolocalizaciones de forma masiva para los arboles necesitaras utilizar esta plantilla para colocar los posiciones.</p>
    <a class="btn" href="/tree/get_template">Descargar Plantilla</a>
        </div>
    <form action="/tree/upload_csv" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <lable>Csv:</label>
        <input type="file" class="btn" name="csv" required>
        <input type="submit" class="btn btn-primary" name="submit" value="Enviar">
    </form>
    </div>
</div>
@endsection