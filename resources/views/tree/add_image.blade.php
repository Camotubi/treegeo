@extends('layouts.app') 
@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Agregar Imagenes al Arbol - {{$tree->common_name}}</div>

            <div class="card-body">
    @include('components.image.create_form')
            </div>
        </div>
    </div>
</div>
@endsection