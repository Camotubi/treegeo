<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Tree::class, function (Faker $faker) {
    return [
        'common_name' => $faker->name,
        'scientific_name' => $faker->name,
        'active' => true,
        'description' => $faker->paragraph
    ];
});
