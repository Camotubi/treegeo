<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserType;
use App\Tree;
use App\Position;
use App\Trail;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if(is_null(UserType::where('name','Admin')->first()))
			UserType::insert(['id'=> 1, 'name' => 'Admin']);
		if(is_null(UserType::where('name','Normal')->first()))
            UserType::insert(['id' => 2, 'name' => 'Normal']);
        if(is_null(UserType::where('name','Prueba')->first()))
        UserType::insert(['id' => 3, 'name' => 'Prueba']);

         User::create(array(
            'email'     => 'admin@admin.com',
            'name'=> 'Roy',
            'password' => bcrypt('12345678'),
            'user_type_id' => 1
        ));

        User::create(array(
            'email'     => 'roy.henriquez@gmail.com',
            'name'=> 'Rodee',
            'password' => bcrypt('12345678'),
            'user_type_id' => 2
        ));

        Tree::create(array(
            'common_name'=>'Cabimo',
            'scientific_name'=>'Copaifera Aromatica',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
        ));
Tree::create(array(
            'common_name'=>'Corotú',
            'scientific_name'=>'Enterolobium Cyclocarpum',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Lluvia de oro',
            'scientific_name'=>'Cassia Fistula',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Toreta',
            'scientific_name'=>'Annona Purpurea',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
)); 
Tree::create(array(
            'common_name'=>'Guasimo',
            'scientific_name'=>'Guazuma Ulmifolia',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Caoba',
            'scientific_name'=>'Swietenia Macrophylla',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Corosito',
            'scientific_name'=>'Sacoglottis Ovicarpa',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Nim/Neem',
            'scientific_name'=>'Azadirachta Indica',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Espave/Javillo',
            'scientific_name'=>'Anacardium Excelsum',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Cedro amargo',
            'scientific_name'=>'Cedrela Odorata',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Jagua',
            'scientific_name'=>'Genipa Americana',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Cocobolo',
            'scientific_name'=>'Dalbergia Retusa',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Guayacan cebolla',
            'scientific_name'=>'Tabebuia Guayacan',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Harino',
            'scientific_name'=>'Andira Inermis',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Laurel',
            'scientific_name'=>'Cordia Alliodora',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Cuipo',
            'scientific_name'=>'Cavanillesia Platanifolia',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Guarumo',
            'scientific_name'=>'Cecropia Insignis',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Guayacanes',
            'scientific_name'=>'Tabebuia Guayacan',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Carricillo',
            'scientific_name'=>'Chusquea Simpliciflora',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Hobo',
            'scientific_name'=>'Spondias Mombin',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Madroño',
            'scientific_name'=>'Macrocnemum Roseum',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Higueron',
            'scientific_name'=>'Ficus insipida',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));
Tree::create(array(
            'common_name'=>'Macano',
            'scientific_name'=>'Diphysa Americana',
            'description'=>'',
            'active'=>1,
            'user_id'=>1,
));


Tree::find(1)->positions()->create(array(
            'lat'=>'9.0247298',
            'lng'=>'-79.534959',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(2)->positions()->create(array(
            'lat'=>'9.023456',
            'lng'=>'-79.532884',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(3)->positions()->create(array(
            'lat'=>'9.0249653',
            'lng'=>'-79.5299606',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(4)->positions()->create(array(
            'lat'=>'9.02331400',
            'lng'=>'-79.53250800',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(5)->positions()->create(array(
            'lat'=>'9.0260969',
            'lng'=>'-79.5344074',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(6)->positions()->create(array(
            'lat'=>'9.0268587',
            'lng'=>'-79.5331353',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(7)->positions()->create(array(
            'lat'=>'9.023298',
            'lng'=>'-79.532633',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(8)->positions()->create(array(
            'lat'=>'9.023303',
            'lng'=>'-79.532814',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(9)->positions()->create(array(
            'lat'=>'9.02327',
            'lng'=>'-79.532824',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(10)->positions()->create(array(
            'lat'=>'9.022905',
            'lng'=>'-79.532969',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(11)->positions()->create(array(
            'lat'=>'9.023433',
            'lng'=>'-79.532832',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(12)->positions()->create(array(
            'lat'=>'9.023445',
            'lng'=>'-79.532987',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(13)->positions()->create(array(
            'lat'=>'9.023271996',
            'lng'=>'-79.53308268',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(14)->positions()->create(array(
            'lat'=>'9.023414',
            'lng'=>'-79.533022',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(15)->positions()->create(array(
            'lat'=>'9.02307',
            'lng'=>'-79.532952',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(16)->positions()->create(array(
            'lat'=>'9.022838',
            'lng'=>'-79.532818',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(17)->positions()->create(array(
            'lat'=>'9.023046',
            'lng'=>'-79.53269',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(18)->positions()->create(array(
            'lat'=>'9.025201',
            'lng'=>'-79.531852',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(19)->positions()->create(array(
            'lat'=>'9.023354',
            'lng'=>'-79.533014',
            'user_id'=>1,
            'active'=>1,
));
Tree::find(20)->positions()->create(array(
            'lat'=>'9.027075',
            'lng'=>'-79.533036',
            'user_id'=>1,
            'active'=>1,
));

Tree::find(21)->positions()->create(array(
    'lat'=>'9.027406', //ESTE ARBOL NO ESTÁ EN EL EXCEL - COORDENADAS BRUJAS (JOBBO)
    'lng'=>'-79.532458',
    'user_id'=>1,
    'active'=>1,
));
Tree::find(22)->positions()->create(array(
    'lat'=>'9.0268469',
    'lng'=>'-79.5338516',
    'user_id'=>1,
    'active'=>1,
));
Tree::find(23)->positions()->create(array(
    'lat'=>'9.00283600', //ESTE ARBOL NO ESTÁ EN EL EXCEL - COORDENADAS BRUJAS (LAUREL)
    'lng'=>'-79.53277800',
    'user_id'=>1,
    'active'=>1,
));
/*
Tree::find(24)->positions()->create(array(
    'lat'=>'9.026391',
    'lng'=>'-79.535115',
    'user_id'=>1,
    'active'=>1,
));
*/

        Trail::create(array(
            'name'=>'Trail 1',
            'description'=>'',
            'user_id'=>1, 
        ));
Trail::create(array(
            'name'=>'Trail 2',
            'description'=>'',
            'user_id'=>1, 
));
Trail::create(array(
            'name'=>'Trail 3',
            'description'=>'',
            'user_id'=>1, 
));
Trail::create(array(
            'name'=>'Trail 4',
            'description'=>'',
            'user_id'=>1, 
));
Trail::create(array(
            'name'=>'Trail 5',
            'description'=>'',
            'user_id'=>1, 
));
Trail::create(array(
            'name'=>'Trail 6',
            'description'=>'',
            'user_id'=>1, 
));

        factory('App\Tree',20)->create(['user_id'=> 1]);
    }
}
