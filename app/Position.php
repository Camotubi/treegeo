<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = "positions";
    protected $fillable = ['lat','lng','active', 'user_id'];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function positionable()
    {
        return $this->morphTo();
   }

   public function positionOrders()
    {
        return $this->hasOne(PositionsOrder::class);
    }
}
