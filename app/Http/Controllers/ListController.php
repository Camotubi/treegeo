<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ListController extends Controller
{
    //
    public function index(){
      $trees = \App\Tree::all();
      return view('pages.list', ['trees' => $trees] );
    }
}
