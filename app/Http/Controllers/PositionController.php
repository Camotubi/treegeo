<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Position;
use App\Tree;
use App\Trail;
use App\User;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $positionable_type = $request->input('parent_type');
        $positionable_id = $request->input('parent_id');
        if ($this->positionableExists($positionable_type, $positionable_id)) {
            $position = new Position;
            $user = Auth::User()->id;
            $position->lat = $request->input('lat');
            $position->lng = $request->input('lng');
            $position->users()->associate($user);
            switch ($positionable_type) {
                case 'tree':
                    Tree::find($positionable_id)->positions()->save($position);
                    return redirect("/tree/$positionable_id")->with('success', 'Posición creada');
                    break;
                
                case 'trail':
                    Trail::find($positionable_id)->positions()->save($position);
                    return redirect("/trail/$positionable_id")->with('success', 'Posición creada');
                    break;
            }
        }else {
            switch ($positionable_type) {
                case 'tree':
                    return redirect("/tree/$positionable_id")->with('error', 'No se pudo crear la posición');
                    break;
                
                case 'trail':
                    return redirect("/trail/$positionable_id")->with('error', 'No se pudo crear la posición');
                    break;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $positionable_type = $request->input('parent_type');
        $positionable_id = $request->input('parent_id');
        if ($this->positionableExists($positionable_type, $positionable_id)) {
            $position = Position::find($id);
            $position->lat = $request->input('lat');
            $position->lng = $request->input('lng');
            switch ($positionable_type) {
                case 'tree':
                    Tree::find($positionable_id)->positions()->save($position);
                    break;
                
                case 'trail':
                    Trail::find($positionable_id)->positions()->save($position);
                    break;
            }
            return redirect("/admin/$positionable_type")->with('success', 'Posición actualizada');

        }else {
            return redirect("/admin/$positionable_type")->with('error', 'No se pudo actualizar la posición');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $position = Position::find($id);
        $position->users()->dissociate();
        $position->delete();
        return redirect("/admin/$positionable_type")->with('success', 'Posición eliminada');
    }

    public function createFromTree($id)
    {
        $parent_id = $id;
        $parent_type = 'tree';
        $action = "/admin/tree/$id/position";
        $request_type = 'create';
        return view('position.create', [
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'action' => $action,
            'request_type' => $request_type
        ]);
    }

    public function createFromTrail($id)
    {
        $parent_id = $id;
        $parent_type = 'trail';
        $action = "/admin/trail/$id/position";
        $request_type = 'create';
        return view('position.create', [
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'action' => $action,
            'request_type' => $request_type
        ]);
    }

    public function editFromTree($tree_id, $position_id)
    {
        $position = Position::find($position_id);
        $parent_id = $tree_id;
        $parent_type = 'tree';
        $action = "/admin/tree/$tree_id/position";
        $request_type = 'edit';
        return view('position.edit', [
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'action' => $action,
            'position' => $position,
            'request_type' => $request_type
        ]);
    }

    public function editFromTrail($trail_id, $position_id)
    {
        $position = Position::find($position_id);
        $parent_id = $trail_id;
        $parent_type = 'trail';
        $action = "/admin/trail/$id/position";
        $request_type = 'edit';
        return view('position.edit', [
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'action' => $action,
            'position' => $position,
            'request_type' => $request_type
        ]);
    }

    public function positionableExists(String $parent_type, int $parent_id): bool
    {
        switch ($parent_type) {
            case 'tree':
                return Tree::where('id', $parent_id)->count();
                break;
            case 'trail':
                return Trail::where('id', $parent_id)->count();
                break;
            default:
                return false;
        }
    }
}


