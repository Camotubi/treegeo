<?php
declare (strict_types = 1);

namespace App\Http\Controllers;

use App\Image;
use App\Tree;
use App\Trail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreImage;
use Illuminate\Support\Facades\Auth;
class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreImage $request)
    {
        $validated = $request->validated();
        if ($validated) {
            $imageable_type = $request->input('parent_type');
            $imageable_id = $request->input('parent_id');
            if ($this->imageableExists($imageable_type, $imageable_id)) {
                $directory = "public/$imageable_type";
                Storage::makeDirectory($directory);
                foreach ($request->file('images') as $imageFile) {
                    $image = new Image();
                    $image->user_id = Auth::User()->id;
                    $image->file_location = $imageFile->store($directory);
                    switch ($imageable_type) {
                        case 'tree':
                        Tree::find($imageable_id)->images()->save($image);
                            break;
                        case 'trail':
                        Trail::find($imageable_id)->images()->save($image);
                            break;
                    }

                }
                switch ($imageable_type) {
                case 'tree':
                    return redirect("/tree/$imageable_id")->with('success', 'Imagenes agregadas');
                    break;

                case 'trail':
                    return redirect("/trail/$imageable_id")->with('success', 'Imagenes agregadas');
                    break;
                }
            }
            switch ($imageable_type) {
            case 'tree':
                return redirect("/tree/$imageable_id")->with('error', 'No se pudieron agregar las imagenes');
                break;

            case 'trail':
                return redirect("/trail/$imageable_id")->with('error', 'No se pudieron agregar las imagenes');
                break;
            }
        }
        switch ($imageable_type) {
        case 'tree':
            return redirect("/tree/$imageable_id")->with('error', 'No se pudieron agregar las imagenes');
            break;

        case 'trail':
            return redirect("/trail/$imageable_id")->with('error', 'No se pudieron agregar las imagenes');
            break;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function updateActive(Request $request, $id)
    {
        $imageable_type = $request->input('parent_type');
        $imageable_id = $request->input('parent_id');
        if ($this->imageableExists($imageable_type, $imageable_id)) {
            $image = Image::find($id);
            $image->active = $request->input('active');
            $image->save();
            switch ($imageable_type) {
                case 'tree':
                    if ($request->input('active')) {
                        return redirect("/tree/$imageable_id")->with('success', 'Imagen activada');
                    }else{
                        return redirect("/tree/$imageable_id")->with('success', 'Imagen desactivada');
                    }
                    break;
                
                case 'trail':
                    if ($request->input('active')) {
                        return redirect("/trail/$imageable_id")->with('success', 'Imagen activada');
                    } else {
                        return redirect("/trail/$imageable_id")->with('success', 'Imagen desactivada');
                    }
                    break;
            }
        }
        switch ($imageable_type) {
            case 'tree':
            if ($request->input('active')) {
                    return redirect("/tree/$imageable_id")->with('error', 'No se pudo activar la imagen');
                } else {
                    return redirect("/tree/$imageable_id")->with('error', 'No se pudo desactivar la imagen');
                }
                break;

            case 'trail':
            if ($request->input('active')) {
                    return redirect("/trail/$imageable_id")->with('error', 'No se pudo activar la imagen');
                } else {
                    return redirect("/trail/$imageable_id")->with('error', 'No se pudo desactivar la imagen');
                }
                break;
        }

    }

    public function updatePrimary(Request $request, $id)
    {
        $imageable_type = $request->input('parent_type');
        $imageable_id = $request->input('parent_id');
        $type = $request->input('type');
        if ($this->imageableExists($imageable_type, $imageable_id)) {
            $oldPrimary = Image::where('type', 'primary')->first();
            $newPrimary = Image::find($id);
            if (is_null($oldPrimary)) {
                $newPrimary->type = "primary";
                $newPrimary->save();
            }elseif ($oldPrimary->id == $newPrimary->id) {
                $oldPrimary->type = "secondary";
                $oldPrimary->save();
            }elseif ($oldPrimary->id !== $newPrimary->id) {
                $oldPrimary->type = "secondary";
                $oldPrimary->save();
                $newPrimary->type = "primary";
                $newPrimary->save();
            }
            switch ($imageable_type) {
                case 'tree':
                    if ($type == "primary") {
                        return redirect("/tree/$imageable_id")->with('success', 'La imagen es ahora primaria');
                    } else {
                        return redirect("/tree/$imageable_id")->with('success', 'La imagen es ahora secundaria');
                    }
                    break;
                
                case 'trail':
                    if ($type == "primary") {
                        return redirect("/trail/$imageable_id")->with('success', 'La imagen es ahora primaria');
                    } else {
                        return redirect("/trail/$imageable_id")->with('success', 'La imagen es ahora secundaria');
                    }
                    break;
            }
        }
        switch ($imageable_type) {
            case 'tree':
                if ($type == "primary") {
                    return redirect("/tree/$imageable_id")->with('error', 'No se pudo cambiar la imagen a primaria');
                } else {
                    return redirect("/tree/$imageable_id")->with('error', 'No se pudo cambiar la imagen a secundaria');
                }
                break;
            
            case 'trail':
                if ($type == "primary") {
                    return redirect("/trail/$imageable_id")->with('error', 'No se pudo cambiar la imagen a primaria');
                } else {
                    return redirect("/trail/$imageable_id")->with('error', 'No se pudo cambiar la imagen a secundaria');
                }
                break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $imageable_type = $request->input('parent_type');
        $imageable_id = $request->input('parent_id');
        if ($this->imageableExists($imageable_type, $imageable_id)) {
            $image = Image::find($id);
            $image->user()->dissociate();
            Storage::delete($image->file_location);
            $image->delete();
             switch ($imageable_type) {
                case 'tree':
                    return redirect("/tree/$imageable_id")->with('success', 'La imagen se elimino');
                    break;
                
                case 'trail':
                    return redirect("/trail/$imageable_id")->with('success', 'La imagen se elimino');
                    break;
            }
        }
        switch ($imageable_type) {
            case 'tree':
                return redirect("/tree/$imageable_id")->with('error', 'No se pudo eliminar la imagen');
                break;

            case 'trail':
                return redirect("/trail/$imageable_id")->with('error', 'No se pudo eliminar la imagen');
                break;
        }
    }

    public function imageableExists(String $parent_type, $parent_id)
    {
        switch ($parent_type) {
            case 'tree':return \App\Tree::where('id',  $parent_id)->count();
                break;
            case 'trail':return \App\Trail::where('id', $parent_id)->count();
                break;
            default:return false;
        }
    }
}
