<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Tree;
use App\Position;

class IndexController extends Controller
{
    //
    public function index(){

      $trees = Tree::where('active',true)
      ->whereHas('positions')->with(['positions' => function($q) {
        $q->where('active',true);
      }])->get();
      return view('welcome', [
        'treePosition' => $trees
      ]);
    }
}
