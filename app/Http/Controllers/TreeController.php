<?php

namespace App\Http\Controllers;

use App\Position;
use App\Tree;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use League\Csv\Writer;
use League\Csv\Reader;

class TreeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
    }

    public function getTemplateTreesCsv()
    {
        return response()->streamDownload(function () {
            $trees = Tree::all();
            $csv = Writer::createFromFileObject(new \SplTempFileObject());
            $csv->insertOne(['id', 'common_name', 'scientific_name', 'lat', 'lng', 'device']);
            foreach ($trees as $tree) {
                $csv->insertOne([$tree->id, $tree->common_name, $tree->scientific_name, '', '', '']);
            }
            echo $csv;
        }, 'Plantilla_de_Geolocalizaciones.csv');
    }

    public function show($id)
    {
        try {
            $tree = Tree::findOrFail($id);
            $positions = $tree->positions()->get();
            $parent_id = $id;
            $parent_type = 'tree';
            $action = "/admin/tree/$id/position";
            return view('tree.show', [
                'tree' => $tree,
                'positions' => $positions,
                'parent_id' => $parent_id,
                'parent_type' => $parent_type,
                'action' => $action,
            ]);
        } catch (ModelNotFoundException $e) {
            return redirect('/tree')->with('error', 'Arbol no encontrado');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $tree = Tree::findOrFail($id);
            $positions = $tree->positions()->get();
            return view('tree.edit', [
                'tree' => $tree,
                'positions' => $positions,
            ]);
        } catch (ModelNotFoundException $e) {
            return redirect('/admin/tree')->with('error', 'Arbol no encontrado');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tree = Tree::find($id);
        if ($request->has('position')) {
            $oldPosition = $tree->positions()
                ->where('active', true)
                ->first();
            $newPosition = Position::where('id', $request->input('position'))->first();
            if (is_null($oldPosition)) {
                $newPosition->active = true;
                $newPosition->save();
            } elseif ($oldPosition->id !== $newPosition->id) {
                $oldPosition->active = false;
                $oldPosition->save();
                $newPosition->active = true;
                $newPosition->save();
            }
        }
        $tree->common_name = $request->input('common_name');
        $tree->scientific_name = $request->input('scientific_name');
        $tree->description = $request->input('description');
        if ($request->input('active')) {
            $tree->active = true;
        } else {
            $tree->active = false;
        }
        $tree->save();
        return redirect('/admin/tree')->with('success', 'Arbol actualizado');
    }

    /**
     * Show create view for resource.
     *
     * @param  \Illuminate\Http\Request  $request
     */

    public function create()
    {
        return view('tree.create');
    }

    public function loadFromCsv(Request $request)
    {
        $csv = Reader::createFromFileObject($request->file('csv')->openFile());
        $csv->setHeaderOffset(0);
        $records = $csv->getRecords();
        $user = Auth::User();
        foreach ($records as $offset => $record) {
            $tree = Tree::find($record['id']);
            $tree->positions()->create(['lat'=>$record['lat'], 'lng' => $record['lng'], 'user_id' => $user->id, 'active' => false]);
        }
        return redirect('/')-> with(['success'=> 'Carga Exitosa']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tree = Tree::find($id);
        $positions = $tree->positions()->get();
        foreach ($positions as $position) {
            $position->users()->dissociate();
            $position->delete();
        }
        $tree->user()->dissociate();
        $tree->delete();
        return redirect('/admin/tree')->with('success', 'Árbol eliminado');

    }

    public function uploadCsv() 
    {
        return view('tree.upload_csv');
    }
    public function store(Request $request)
    {
        if (Auth::check() && Auth::User()->isAdmin()) {
            $tree = new Tree();
            $tree->common_name = $request->input('common_name');
            $tree->scientific_name = $request->input('scientific_name');
            $tree->description = $request->input('description');
            if ($request->has('active')) {
                $tree->active = true;
            } else {
                $tree->active = false;
            }
            $tree->user_id = Auth::User()->id;
            $tree->save();
            return redirect("/tree/$tree->id");
        }
        return redirect("/admin/tree")->with('error', 'Usted no es administrador');
    }

    public function addPosition($id)
    {
        $parent_id = Tree::find($id);
        $parent_type = 'tree';
        return view('position.create', [
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
        ]);
    }

    public function addImage($id): View
    {
        return view('tree.add_image', ['tree' => Tree::findOrFail($id), 'action' => '/image', 'parent_id' => $id, 'parent_type' => 'tree']);
    }
}
