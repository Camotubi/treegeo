<?php

namespace App\Http\Controllers;

use App\Position;
use App\Trail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PositionsOrder;

class TrailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trails = Trail::all();
        return view('trail.index', ['trails' => $trails]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('trail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $trail = new Trail;
        $user_id = Auth::User()->id;
        $trail->name = $request->input('name');
        $trail->description = $request->input('description');
        $trail->user()->associate($user_id);
        $trail->save();
        return redirect('/admin/trail')->with('success', 'Sendero creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trail = Trail::find($id);
        $positions = $trail->positions()->get();
        $parent_id = $id;
        $parent_type = 'trail';
        $action = "/admin/trail/$id/position";
        return view('trail.show', [
            'trail' => $trail,
            'positions' => $positions,
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'action' => $action,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trail = Trail::find($id);
        $groupIds = $trail->positions()->whereHas('positionOrders')
        ->with('positionOrders')->get()->pluck('positionOrders.group_id')->unique();
        return view('trail.edit', [
            'trail' => $trail,
            'groupIds' => $groupIds
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $trail = Trail::find($id);
        $newActives = $trail->positionOrders()->where('group_id', $request->input('group_id'))->get();
        $oldActives = $trail->positions()->where('active', true)->get();
        foreach ($oldActives as $oldActive) {
            $oldActive->active = false;
            $oldActive->save();
        }
        foreach ($newActives as $newActive) {
            $position = $trail->positions()->where('id', $newActive->position_id)->first();
            $position->active = true;
            $position->save();
        }
        $trail->name = $request->input('name');
        $trail->description = $request->input('description');
        $trail->save();
        return redirect('/admin/trail')->with('success', 'Sendero actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trail = Trail::find($id);
        $positionOrders = $trail->positionOrders()->get();
        foreach ($positionOrders as $positionOrder) {
            $positionOrder->trail()->dissociate();
            $positionOrder->position()->dissociate();
            $positionOrder->delete();
        }
        $positions = $trail->positions()->get();
        foreach ($positions as $position) {
            $position->users()->dissociate();
            $position->delete();
        }
        $trail->user()->dissociate();
        $trail->delete();
        return redirect('/admin/trail')->with('success', 'Sendero eliminado');
    }

   /* public function addPosition($id)
    {
        $parent_id = Trail::find($id);
        $parent_type = 'trail';
        return view('position.create', [
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
        ]);
    }*/

    public function addPositions(Request $request, $id)
    {
        $trail = Trail::find($id);
        $lats = $request->input('lat');
        $lngs = $request->input('lng');
        $devices = $request->input('devices');
        $user = Auth::User();
        $groupId = 0;
        if(PositionsOrder::count()) {
            $groupId = PositionsOrder::orderBy('group_id','desc')->first()->group_id + 1;
        }
        for ($i = 0; $i < sizeof($lats); $i++) {
            $position = $trail->positions()->create([
                'lat' => $lats[$i],
                'lng' => $lngs[$i],
                'device' => $devices[$i],
                'user_id' => $user->id,
            ]);
            $position->positionOrders()->create([
                'group_id' => $groupId,
                'trail_id' => $trail->id,
                'order' => $i,
            ]);
        }
        return redirect("/trail/$id")->with('success', 'Posiciones agregadas');
    }

}
