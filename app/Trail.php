<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trail extends Model
{
    protected $table = "trails";

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function positions()
    {
        return $this->morphMany(Position::class, 'positionable');
    }

    public function positionOrders()
    {
        return $this->hasMany(PositionsOrder::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class,'imageable');
    }
}
