<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','user_type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin() {
        return $this->user_type_id === UserType::where('name','Admin')->first()->id;
    }
    public function type()
    {
        return $this->belongsTo(UserType::class, 'user_type_id');
    }

    public function positions()
    {
        return $this->hasMany(Position::class);
    }

    public function trees()
    {
        return $this->hasMany(Tree::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function trails()
    {
        return $this->hasMany(Trail::class);
    }
}
