<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function imageable()
    {
        $this->morphTo();
    }

    public function getPublicFileLocationAttribute()
    {
        return Storage::url($this->file_location);
    }

    public function isPrimary()
    {
        return $this->type === 'primary';
    }
    public function isSecondary()
    {
        return $this->type === 'secondary';
    }

}
