<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PositionsOrder extends Model
{
    protected $table = "trails_positions";
    protected $fillable = ['trail_id','group_id','position_id','order'];
    public function trail()
    {
        return $this->belongsTo(Trail::class);
    }

    public function position()
    {
        return $this->belongsTo(Position::class);
    }

}
