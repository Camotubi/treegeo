<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Tree extends Model
{
    protected $table = "trees";

    protected $fillable = [
        'common_name', 'scientific_name', 'description','user_id','active'
    ];

	public function positions(){
        return $this->morphMany(Position::class, 'positionable');
    }

    public function user(){
        return $this->belongsto(User::class, 'user_id');
    }

    public function images() {
        return $this->morphMany(Image::class,'imageable');
    }
}
