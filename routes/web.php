<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'IndexController@index');

Route::get('/list', 'ListController@index');

Auth::routes();

//Custom Routes
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/usersdash', 'AdminController@index')->name('dash');
//User
Route::resource('user', 'UserController');
//Image
Route::resource('image','ImageController')->except(['update']);
Route::patch('/image/{id}/updateActive', 'ImageController@updateActive');
Route::patch('/image/{id}/updatePrimary', 'ImageController@updatePrimary');

//AdminTree
Route::get('/admin/tree/create', 'TreeController@create');
Route::get('/admin/tree', 'AdminController@treeIndex');
Route::get('/admin/tree/{id}/edit', 'TreeController@edit');
Route::post('/tree', 'TreeController@store');
Route::patch('/admin/tree/{id}', 'TreeController@update');
//AdminTrail
Route::get('/admin/trail/create', 'TrailController@create');
Route::get('/admin/trail', 'TrailController@index');
Route::get('/admin/trail/{id}/edit', 'TrailController@edit');
Route::post('/admin/trail', 'TrailController@store');
Route::patch('/admin/trail/{id}', 'TrailController@update');

//Positions
//Tree
Route::get('/admin/tree/{id}/position/create', 'PositionController@createFromTree');
Route::post('/admin/tree/{id}/position', 'PositionController@store');
Route::get('/admin/tree/{tree_id}/position/{position_id}/edit', 'PositionController@editFromTree');
Route::patch('/admin/tree/{id}/position', 'PositionController@update');
//Trail
Route::get('/admin/trail/{id}/position/create', 'PositionController@createFromTrail');
Route::post('/admin/trail/{id}/position', 'PositionController@store');
Route::get('/admin/trail/{id}/position/{position_id}/edit', 'PositionController@editFromTrail');
Route::patch('/admin/trail/{id}/position', 'PositionController@update');


//Tree
Route::get('/tree/upload_csv','TreeController@uploadCsv');
Route::post('/tree/upload_csv','TreeController@loadFromCsv');
Route::get('/tree/get_template', 'TreeController@getTemplateTreesCsv');
Route::get('/tree/{id}', 'TreeController@show');
Route::get('/tree/{id}/add_image', 'TreeController@addImage');
Route::resource('tree', 'TreeController')->only([
    'destroy'
]);
//Route::delete('/tree/{id}', 'TreeController@destroy');
//Trail
Route::get('/trail/{id}', 'TrailController@show');
Route::delete('/trail/{id}', 'TrailController@destroy');
Route::post('/trail/{id}/add_positions', 'TrailController@addPositions');

