var map;

$(document).ready(function() {
var box;

	initMap(17.5, 9.0238187, -79.5337285, "PARQUE SUR", "1");

    // $('.has-pointer').click(function() {
		// var lon = $(this).attr("data-lon");
		// var lat = $(this).attr("data-lat");
		// var title = $(this).attr("data-title");
    //     var info = $(this).attr("data-info");
		// initMap(15, lat, lon, title, info);
		//
		//
    // });


  function initMap(valZoom, lat, lon, valTitle, info) {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: valZoom,
                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(lat, lon),
					panControl: false,
					zoomControl: true,
					scrollwheel: false,
					zoomControlOptions: {
						position: google.maps.ControlPosition.LEFT_BOTTOM
					},
					mapTypeControl: false,
					scaleControl: false,
					streetViewControl: false,
					overviewMapControl: false,

                    // How you would like to style the map.
                    styles:
                    [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#009fdf"
            },
            {
                "visibility": "on"
            }
        ]
    }
]};


                var mapElement = document.getElementById('map');


                var map = new google.maps.Map(mapElement, mapOptions);

                var url = location.href;

                    // Mark Español
                      // Mark 2

                    // var contentString2 = '<div id="content">'+
                    //   '<div id="siteNotice">'+
                    //   '</div>'+
										// 	'<h6 style="margin-bottom: 5px;color: #008825;"id="firstHeading" class="firstHeading font-weight-text-700">Geo Tree</h6>'+
                    //   '<h5 style="margin-bottom: -10px;"id="firstHeading" class="firstHeading">Árbol Panamá</h5>'+
                    //   '<span style="font-weight:200;margin-top:5px;display:block; font-size:12px; color:#009fdf;">(Sterculia apetala)</span>'+
                    //   '<hr>' +
                    //   '<div id="bodyContent">'+
										// 	'<p class="margin-all-10" style="line-height: 14px;"><b>Author: </b>Roderik Acevedo</p>' +
                    //   '</div>'+
                    //   '</div>';
										//
                    // var infowindow2 = new google.maps.InfoWindow({
                    //   content: contentString2
                    // });

                    // var marker1 = new google.maps.Marker({
                    //     position: new google.maps.LatLng(9.0238187, -79.5337285),
                    //     map: map,
                    //     title: 'PARQUE SUR',
                    //     icon:'/img/logo/pin.png'
                    // });

                    // google.maps.event.addListener(marker1, 'click', function() {
                    //     infowindow2.open(map,marker1);
                    //   });
										//
                    //   if (info == "1") {
                    //       infowindow2.open(map,marker1);
                    //   }

                    //
                    //
                    // if (info == "1"){
                    //   infowindow.open(map,marker);
                    // }else if (info == "2"){
                    //   infowindow2.open(map,marker2);
                    // }else if (info == "3"){
                    //   infowindow3.open(map,marker3);
                    // }else if (info == "4"){
                    //   infowindow4.open(map,marker4);
                    // }

										//LARAVEL COMUNCATION

										$('span').each(function(index, element){
											$( element ).css( "display", "none" );
											var id = $(this).attr('treeId');
											var title = $(this).attr('title');
											var lat = $(this).attr('lat');
											var long = $(this).attr('long');

											// box
											var contentData = '<div id="content">'+
												'<div id="siteNotice">'+
												'</div>'+
												'<h6 style="margin-bottom: 5px;color: #008825;"id="firstHeading" class="firstHeading font-weight-text-700">Geo Tree</h6>'+
												'<h5 style="margin-bottom: -10px;"id="firstHeading" class="firstHeading">'+title+'</h5>'+
												'<hr>' +
												'<div id="bodyContent">'+
												'<a href="/tree/'+id+'" target="_self">Ver Mas</a>' +
												'</div>'+
												'</div>';
												var infowindow = new google.maps.InfoWindow({
													content: contentData
												});

											var box = [{
												title:title,
												latitud:lat,
												longitud:long
											}
											];
											element = new google.maps.Marker({
	                        position: new google.maps.LatLng(box[0].latitud,box[0].longitud),
	                        title: box[0].title,
	                        icon:'/img/logo/pin.png'
	                    });

											google.maps.event.addListener(element, 'click', function() {
	                        infowindow.open(map,element);
	                      });


											element.setMap(map);
											console.log(box[0]);
										});

            }

});
